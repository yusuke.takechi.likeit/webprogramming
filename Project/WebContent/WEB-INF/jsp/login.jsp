<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
 <link href="css/login.css" rel="stylesheet">
</head>

<body class="text-center">

    <form class="form-signin" action="LoginServlet" method="post">
   <br>
  <h1 class="h3 mb-3 font-weight-normal">ログイン画面</h1>
  <br>
  <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger text-center" role="alert">
		  ${errMsg}
		</div>
	</c:if>
  <label for="inputEmail" class="sr-only">ログインID</label>
  <input type="text" name="loginId" id="inputEmail" class="form-control" placeholder="ID" required autofocus>
  <br>
  <br>
  <label for="inputPassword" class="sr-only">パスワード</label>
  <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
  <br>
  <br>
    <button class="btn btn-lg btn-primary btn-block" type="submit">ログイン</button>
</form>
</body>
</html>