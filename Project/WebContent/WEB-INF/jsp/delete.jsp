<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>削除</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/delete.css">
</head>
<body>
	<nav class="navbar navbar-light bg-dark">
    <li class="nav-item">
 	<a class="text-success">${userinfo.name}さん</a>
    </li>
  <a class="navbar-brand"> </a>
  <form class="form-inline">
    <a class="btn btn-outline-success my-2 my-sm-0" href="LogoutServlet">ログアウト</a>
  </form>
</nav>
<br>
<h1 class="text-center">ユーザ削除確認</h1>
<br>
<br>
<br>
	 <br>
	<form class="ask" action="UserdetailServlet" method="post">
	ログインID:"${info.loginId}" <br>
	<input type="hidden" name=loginId value="${info.loginId}">
	を本当に削除してよろしいしょうか。
	<br>
	<br>
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	&emsp;&emsp;&emsp;
	<button id="cancel" class="cancel" value="キャンセル">キャンセル</button>
	&emsp;&emsp;&emsp;&emsp;
	<button name="OK" class="ok" value="OK">OK</button>
	</form>
</body>
</html>