<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>user</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/list.css" rel="stylesheet">
</head>
<body >
<nav class="navbar navbar-light bg-dark">

 	<p class="text-success">${userinfo.name}さん</p>

  <a class="navbar-brand"> </a>
  <form class="form-inline">
    <a  class="btn btn-outline-success my-2 my-sm-0" href="LogoutServlet">ログアウト</a>
  </form>
</nav>
<br>
<h1 class="text-center">ユーザ一覧</h1>
<br>
<a class="new-account" href="new_account">新規登録</a>
<br>
<form action="UserListServlet" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">ログインID</label>
    <input type="text" class="form-control" name= "loginid" >
    <br>
    <label for="exampleInputPassword1">ユーザ名</label>
    <input type="text" class="form-control" name="name" >
    <br>
      <label for="date">生年月日</label><br>
      <input type="date" id="date" name="fromBirthDate" />&emsp;～&emsp;<input type="date" id="date" name="toBirthDate" />
     </div>
  <br>
  <div class=botan>
  <button type="submit" class="btn btn-primary">検索</button>
</div>
</form>
<br>
<br>
<div class="container">
<form action="UserListServlet" method="get">
	<table class="table">
	  <thead class="thead-dark">
	    <tr class="text-center">
	      <th scope="col">ログインID </th>
	      <th scope="col">ユーザ名</th>
	      <th scope="col">生年月日</th>
	      <th scope="col">　</th>
	    </tr>
	  </thead>
	  <tbody>
	   <c:if test="${userinfo.loginId=='admin'}">
	<c:forEach var="user" items="${userList}" >
	    <tr class= "text-center">
	      <th scope="row">${user.loginId}</th>
	      <td>${user.name}</td>
	      <td>${user.birthDate}</td>
	      <td>
	      	<div class="btn-group" role="group" aria-label="Basic example">
  <a href="UserInfoServlet?id=${user.id}" class="btn btn-secondary">参照</a>
  <a href="UserupdateServlet?id=${user.id}" class="btn btn-primary">更新</a>
  <a href="UserdetailServlet?id=${user.id}" class="btn btn-success">削除</a>
</div>
	      </td>
	    </tr>
	</c:forEach>
	</c:if>
	 <c:if test="${userinfo.loginId!='admin'}">
	  <c:forEach var="user" items="${userList}" >
	    <tr class= "text-center">
	      <th scope="row">${user.loginId}</th>
	      <td>${user.name}</td>
	      <td>${user.birthDate}</td>
	      <td>
	      	<div class="btn-group" role="group" aria-label="Basic example">
  <a href="UserInfoServlet?id=${user.id}" class="btn btn-secondary">参照</a>
 	 <c:if test="${userinfo.loginId==user.loginId}">
  <a href="UserupdateServlet?id=${user.id}" class="btn btn-primary">更新</a>
  	</c:if>
</div>
	      </td>
	    </tr>
	</c:forEach>
	</c:if>
	  </tbody>
	</table>
	</form>
</div>
</body>
</html>