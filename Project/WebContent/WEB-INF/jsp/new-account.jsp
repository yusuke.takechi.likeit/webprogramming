<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>new accounts</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/account.css">
</head>
<body>

<nav class="navbar navbar-light bg-dark">
    <li class="nav-item">
 	<a class="text-success">${userinfo.name}さん</a>
    </li>
  <a class="navbar-brand"> </a>
  <form class="form-inline">
    <a class="btn btn-outline-success my-2 my-sm-0" href="LogoutServlet">ログアウト</a>
  </form>
</nav>
<br>
<h1 class="text-center">ユーザ新規登録</h1>
<br>
<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger text-center" role="alert">
		  ${errMsg}
		</div>
	</c:if>
<br>
<br>
	 <br>
<form class="format" action="new_account" method="post">
  	ログインID&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<input type = "text" size="20" name="loginId"><br>
  	<br>
  	パスワード&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<input type = "text" name="password"><br>
  	<br>
  	パスワード確認&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<input type = "text" name="password2"><br>
  	<br>
	 ユーザ名 &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<input type = "text" name="name"><br>
	 <br>
	  生年月日&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<input type = "text" name="birthDate"><br>
	     <br>
	     <br>

   <div class="botan">
    <button class="btn btn-lg btn-primary btn-block" type="submit">登録</button>
    </div>
    </form>
   <br>
    <a href="UserListServlet" class="back"> 戻る</a>
</body>
</html>