<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>information</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/account.css">
<body>

<nav class="navbar navbar-light bg-dark">
 	<a class="text-success">${userinfo.name}さん</a>
  <a class="navbar-brand"> </a>
  <form class="form-inline">
    <a class="btn btn-outline-success my-2 my-sm-0" href="LogoutServlet">ログアウト</a>
  </form>
</nav>
<br>
<h1 class="text-center">ユーザ情報</h1>
<br>
<br>
<br>
	 <br>
<form class="format">
  	ログインID&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;${info.loginId}<br>
  	<br>
  		ユーザ名&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;${info.name}<br>
  	<br>
  		生年月日&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;${info.birthDate}<br>
  	<br>
	 登録日時 &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;${info.createDate}<br>
	 <br>
	 更新日時&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;${info.updateTime}<br>
 </form>
	     <br>
    <br>
   <a class="back" href="UserListServlet"> 戻る</a>
</body>
</html>