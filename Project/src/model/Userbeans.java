package model;

import java.io.Serializable;

public class Userbeans implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private String birthDate;
	private String password;
	private String createDate;
	private String updateTime;
	

		public Userbeans(String loginId, String name) {
			this.loginId = loginId;
			this.name = name;
		}

		
		public Userbeans(int id, String loginId, String name, String birthDate, String password, String createDate,
				String updateTime) {
			this.id = id;
			this.loginId = loginId;
			this.name = name;
			this.birthDate = birthDate;
			this.password = password;
			this.createDate = createDate;
			this.updateTime = updateTime;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getLoginId() {
			return loginId;
		}
		public void setLoginId(String loginId) {
			this.loginId = loginId;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getBirthDate() {
			return birthDate;
		}
		public void setBirthDate(String birthDate) {
			this.birthDate = birthDate;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getCreateDate() {
			return createDate;
		}
		public void setCreateDate(String createDate) {
			this.createDate = createDate;
		}
		public String getUpdateTime() {
			return updateTime;
		}
		public void setUpdateTime(String updateTime) {
			this.updateTime = updateTime;
		}

		public Userbeans( String loginId, String name, String birthDate, String password, String createDate, String updateTime) {
			this.loginId = loginId;
			this.name = name;
			this.birthDate = birthDate;
			this.password = password;
			this.createDate=createDate;
			this.updateTime=updateTime;
			
		}
		
		public Userbeans( String loginId, String name, String birthDate,String createDate,
				String updateTime) {
			this.loginId = loginId;
			this.name = name;
			this.birthDate = birthDate;
			this.updateTime = updateTime;
			this.createDate=createDate;
			
		}
		
}