package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.Userbeans;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session=request.getSession();
		if(session.getAttribute("userinfo")!=null) {
			response.sendRedirect("UserListServlet");
		}else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
    		dispatcher.forward(request, response);
	     }
		System.out.println(session.getAttribute("userinfo"));
		
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	     request.setCharacterEncoding("UTF-8");
	     
	    	 
	     String loginId=request.getParameter("loginId");
	     String password=request.getParameter("password");
	     
	     UserDao userDao= new UserDao();
	     Userbeans user=userDao.findByLoginInfo(loginId, password);
	     
	     if(user==null) {
	    	 request.setAttribute("errMsg", "ログインに失敗しました。");
	    	 RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
	    	 dispatcher.forward(request, response);
	     }
	     
	     HttpSession session=request.getSession();
	    session.setAttribute("userinfo", user);
	    
	    response.sendRedirect("UserListServlet");
	     }
	}
	


