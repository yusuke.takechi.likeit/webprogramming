package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.Userbeans;

/**
 * Servlet implementation class UserdetailServlet
 */
@WebServlet("/UserdetailServlet")
public class UserdetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserdetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String id = request.getParameter("id");

	     UserDao UserDao= new UserDao();
	 	Userbeans user = UserDao.info(id);

		request.setAttribute("info", user);
		
		HttpSession session= request.getSession();
		if(session.getAttribute("userinfo")!=null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/delete.jsp");
			dispatcher.forward(request, response);
		}else {
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
    		dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  String loginId=request.getParameter("loginId");
		  System.out.println(request.getParameter("OK"));
		  	if(request.getParameter("OK")!=null) {
		     
		     UserDao UserDao= new UserDao();
		     UserDao.delete(loginId);

		     response.sendRedirect("UserListServlet");
		  	}else{
		  		response.sendRedirect("UserListServlet");
		  	}
		
	}

}
