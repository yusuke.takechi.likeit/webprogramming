package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.Userbeans;

/**
 * Servlet implementation class UserupdateServlet
 */
@WebServlet("/UserupdateServlet")
public class UserupdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserupdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session= request.getSession();
		if(session.getAttribute("userinfo")!=null) {
			String id = request.getParameter("id");

		     UserDao UserDao= new UserDao();
		 	Userbeans user = UserDao.info(id);

			request.setAttribute("info", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);

		}else {
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
    		dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
	     String name=request.getParameter("name");
	     String password=request.getParameter("password");
	     String password2=request.getParameter("password2");
	     String birthDate=request.getParameter("birthDate");
	     String loginId=request.getParameter("loginId");
	     String id = request.getParameter("id");
	     
	     if(name.equals("")||!password.equals(password2)||birthDate.equals("")) {
	    	 request.setAttribute("errMsg", "入力された内容は正しくありません");

		     UserDao UserDao= new UserDao();
		 	Userbeans user = UserDao.info(id);
		 	
		 	request.setAttribute("info", user);


				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
				dispatcher.forward(request, response);

	    	 
	     }else if(password.equals("")&password2.equals("")){
	    	 UserDao UserDao= new UserDao();
	    	  UserDao.update(loginId, name, password, birthDate);
	     }else{ 
	     UserDao UserDao= new UserDao();
	     UserDao.update(loginId, name, password, birthDate);
	     }
	     response.sendRedirect("UserListServlet");
	     }

	}


