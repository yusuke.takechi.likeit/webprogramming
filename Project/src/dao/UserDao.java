package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.Userbeans;

public class UserDao {
	public Userbeans findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, md5(password));
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new Userbeans(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<Userbeans> findAll() {
		Connection conn = null;
		List<Userbeans> userList = new ArrayList<Userbeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user where login_id <> 'admin'";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String birthDate = rs.getString("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateTime = rs.getString("update_time");
				Userbeans user = new Userbeans(id, loginId, name, birthDate, password, createDate, updateTime);

				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public void newaccount(String name, String password, String birthDate, String loginId, String createDate,
			String updateTime) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "insert into user (login_id,name,birth_date,password,create_date,update_time)values(?,?,?,?,now(),now())";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(2, name);
			pStmt.setString(4, md5(password));
			pStmt.setString(3, birthDate);
			pStmt.setString(1, loginId);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public Userbeans info(String id) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT login_id,name,birth_date,create_date,update_time FROM user where id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			String birthDate = rs.getString("birth_date");
			String createDate = rs.getString("create_date");
			String updateTime = rs.getString("update_time");

			return new Userbeans(loginId, name, birthDate, createDate, updateTime);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;
	}

	public void update(String loginId, String name, String password, String birthDate) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "update user set name=?,password=?,birth_date=?,update_time=now() where login_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, name);
			pStmt.setString(2, md5(password));
			pStmt.setString(3, birthDate);
			pStmt.setString(4, loginId);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void delete(String loginId) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "delete from user where login_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, loginId);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	

	public List<Userbeans> searchy(String loginid, String name, String fromBirthDate, String toBirthDate) {
		Connection conn = null;
		List<Userbeans> list = new ArrayList<Userbeans>();
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user where login_id <> 'admin' ";

			if (!"".equals(loginid)) {
				sql += " and login_id = '" + loginid + "'";

			}
			if (!"".equals(name)) {
				sql += " and name like '" + "%" + name + "%" + "'";

			}
			if (!"".equals(fromBirthDate)) {
				sql += " and '" + fromBirthDate + "' <= birth_date ";

			}
			if (!"".equals(toBirthDate)) {
				sql += " and '" + toBirthDate + "' >= birth_date";
			}
			System.out.println(sql);

			Statement Stmt = conn.createStatement();

			ResultSet rs = Stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String Name = rs.getString("name");
				String BirthDate = rs.getString("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateTime = rs.getString("update_time");
				Userbeans user = new Userbeans(id, loginId, Name, BirthDate, password, createDate, updateTime);

				list.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return list;
	}

	String md5(String password) {
		String source=password;
		Charset charset=StandardCharsets.UTF_8;
		String algorithm="MD5";

		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result=DatatypeConverter.printHexBinary(bytes);
		return result;


	}
}
